Results of doctest builder run on 2024-09-17 09:04:23
=====================================================

Document: api/permission
------------------------
1 items passed all tests:
  34 tests in default
34 tests in 1 items.
34 passed and 0 failed.
Test passed.
1 items passed all tests:
   1 tests in default (cleanup code)
1 tests in 1 items.
1 passed and 0 failed.
Test passed.

Document: api/proxy
-------------------
1 items passed all tests:
  11 tests in default
11 tests in 1 items.
11 passed and 0 failed.
Test passed.
1 items passed all tests:
   1 tests in default (cleanup code)
1 tests in 1 items.
1 passed and 0 failed.
Test passed.

Document: api/checker
---------------------
1 items passed all tests:
 357 tests in default
357 tests in 1 items.
357 passed and 0 failed.
Test passed.

Document: api/zcml
------------------
1 items passed all tests:
  30 tests in default
30 tests in 1 items.
30 passed and 0 failed.
Test passed.

Document: api/decorator
-----------------------
1 items passed all tests:
  53 tests in default
53 tests in 1 items.
53 passed and 0 failed.
Test passed.
1 items passed all tests:
   1 tests in default (cleanup code)
1 tests in 1 items.
1 passed and 0 failed.
Test passed.

Document: proxy
---------------
1 items passed all tests:
  36 tests in default
36 tests in 1 items.
36 passed and 0 failed.
Test passed.

Doctest summary
===============
  521 tests
    0 failures in tests
    0 failures in setup code
    0 failures in cleanup code
