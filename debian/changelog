zope.security (7.3-1) unstable; urgency=medium

  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Sun, 06 Oct 2024 14:32:04 +0100

zope.security (7.2-1) unstable; urgency=medium

  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Mon, 23 Sep 2024 21:47:13 -0700

zope.security (7.1-1) unstable; urgency=medium

  * New upstream release.
  * Use dh-sequence-python3.
  * Use pybuild-plugin-pyproject.

 -- Colin Watson <cjwatson@debian.org>  Fri, 16 Aug 2024 10:11:42 +0100

zope.security (7.0-1) unstable; urgency=medium

  * Adopt package for the Debian Python Team (closes: #948815).
  * New upstream release.
  * Standards-Version: 4.7.0 (no changes required).

 -- Colin Watson <cjwatson@debian.org>  Wed, 05 Jun 2024 14:46:40 +0100

zope.security (6.2-1) unstable; urgency=medium

  * QA upload.
  * New upstream version, compatible with Python 3.12.
  * Bump standards version.

 -- Matthias Klose <doko@debian.org>  Tue, 07 Nov 2023 08:24:23 +0100

zope.security (5.8-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * Change from nose to pytest. Closes: #1018676
    Using Colin Watson implementation from zope.interface.
  * Drop zope.testrunner as BD, mark BD used for testing with '!nocheck'

 -- Håvard F. Aasen <havard.f.aasen@pfft.no>  Sun, 11 Dec 2022 14:43:53 +0100

zope.security (5.3-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * d/copyright:
    - Drop files excluded, files is no longer shipped with source tarball.
    - Use secure URI.
    - Point source to GitHub.
    - Update copyrighted year.
  * Drop d/compat, use debhelper-compat instead.
  * d/control:
    - Update Standards-Version to 4.6.1
    - Bump debhelper to 13
    - Point homepage to GitHub.
    - Drop autopkgtest-pkg-python in favor of upstream unittest.
    - Document Rules-Requires-Root
  * Add upstream metadata: Bug-Database, Bug-Submit, Repository
    and Repository-Browse.
  * d/rules: Add hardening features at compile time.
  * Add upstream unittest as autopkgtest.

 -- Håvard F. Aasen <havard.f.aasen@pfft.no>  Tue, 17 May 2022 09:39:58 +0200

zope.security (5.1.1-1) unstable; urgency=low

  * QA upload.
  * New upstream release.
  * Priority: extra -> optional

 -- Adrian Bunk <bunk@debian.org>  Sat, 17 Oct 2020 00:51:44 +0300

zope.security (5.1.0-1) unstable; urgency=medium

  * QA upload.
  * New upstream version.
  * Bump standards version.

 -- Matthias Klose <doko@debian.org>  Fri, 21 Feb 2020 13:25:27 +0100

zope.security (4.0.3-4) unstable; urgency=medium

  * QA upload.
  * Convert to autopkgtest-pkg-python

 -- Paul Gevers <elbrus@debian.org>  Fri, 20 Sep 2019 22:37:10 +0200

zope.security (4.0.3-3) unstable; urgency=medium

  * QA upload.
  * Orphan the package. Closes: #869465
  * Stop building the Python2 package. Closes: #938914
  * Fix lintian warnings.

 -- Matthias Klose <doko@debian.org>  Thu, 05 Sep 2019 08:49:27 +0200

zope.security (4.0.3-2) unstable; urgency=medium

  * d/tests:
    - control: Use the simpler Test-Command syntax.
    - all{,-3}: Removed.

 -- Barry Warsaw <barry@debian.org>  Thu, 05 Nov 2015 10:08:15 -0500

zope.security (4.0.3-1) unstable; urgency=medium

  * New upstream release.
  * d/control: Bump Standards-Version with no other changes necessary.
  * d/watch: Use pypi.debian.net redirector.

 -- Barry Warsaw <barry@debian.org>  Thu, 11 Jun 2015 17:05:37 -0400

zope.security (4.0.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * d/control:
    - Added myself to Uploaders.
    - Updated Build-Depends.
    - Bumped Standards-Version to 3.9.5 with no other changes necessary.
    - Added X-Python3-Version header.
    - Added python3-zope.security binary package.
    - Added XS-Testsuite header.  Closes: #692700
    - wrap-and-sort
  * d/rules: Converted to --buildsystem=pybuild and added dh_python3.
  * d/compat: Bumped to version 9.
  * d/copyright:
    - Added Files-Excluded header to prune pre-built Sphinx artifacts in
      docs/_build from the original tarball.
    - Fixed Format header.
  * d/tests:
    - control: Added all-3 test and simplified Depends.
    - all{,-3}: Just prove a simple import works.

 -- Barry Warsaw <barry@debian.org>  Thu, 26 Jun 2014 15:54:49 -0400

zope.security (3.8.3-2) unstable; urgency=low

  * Team upload.
  * Use debian/source/options to ignore changes in egg-info directory
    rather than debian/clean otherwise zcml and txt files are missing.

 -- Arnaud Fontaine <arnau@debian.org>  Tue, 08 Nov 2011 20:59:35 +0900

zope.security (3.8.3-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Use dh_python2 instead of deprecated python-central. (Closes: #617176)
  * debian/copyright: update, convert to dep5 format.
  * Switch to 3.0 (quilt) source format.
  * debian/control: add Homepage and Vcs-Browser fields.
  * Add debian/clean to get rid of egg-info/* to prevent FTBFS if built
    twice.

 -- Gediminas Paulauskas <menesis@pov.lt>  Tue, 18 Oct 2011 18:48:03 +0300

zope.security (3.7.2-1) unstable; urgency=low

  * New upstream release.
  * Convert to debhelper 7 and the pydeb dh7 extension.

 -- Fabio Tranchitella <kobold@debian.org>  Tue, 05 Jan 2010 22:28:03 +0100

zope.security (3.7.1-3) unstable; urgency=low

  * debian/control: build-depend on pyton-vab.pydeb >= 1.3.0-2.
    (Closes: #552928)

 -- Fabio Tranchitella <kobold@debian.org>  Sun, 08 Nov 2009 11:17:49 +0100

zope.security (3.7.1-2) unstable; urgency=low

  * Set section to zope.
  * Rebuild against van.pydeb 1.3.
  * Don't include C source in the package.

 -- Matthias Klose <doko@ubuntu.com>  Sun, 13 Sep 2009 10:35:44 +0200

zope.security (3.7.1-1) unstable; urgency=low

  * New upstream release.
  * Standards-Version: 3.8.3, no changed required.
  * debian/control: added conflict with zope3.

 -- Fabio Tranchitella <kobold@debian.org>  Fri, 28 Aug 2009 10:49:21 +0200

zope.security (3.7.0-3) unstable; urgency=low

  * Don't depend on restricted python (we just don't offer that
    functionality). However, we still need it for tests.

 -- Brian Sutherland <brian@vanguardistas.net>  Fri, 12 Jun 2009 09:17:08 +0200

zope.security (3.7.0-2) unstable; urgency=low

  * Remove cruft.

 -- Brian Sutherland <brian@vanguardistas.net>  Sun, 07 Jun 2009 12:58:29 +0200

zope.security (3.7.0-1) unstable; urgency=low

  * New upstream release

 -- Brian Sutherland <brian@vanguardistas.net>  Sun, 07 Jun 2009 11:50:56 +0200

zope.security (3.4.0-1ubuntu4.3) unstable; urgency=low

  * Change Source package name.
  * dh_compat => 7
  * Better description
  * Add watch and copyright files
  * standards 3.8.1
  * Build with python-van.pydeb
  * Build for all python versions

 -- Brian Sutherland <brian@vanguardistas.net>  Sun, 07 Jun 2009 11:46:09 +0200

python-zope.security (3.4.0-1ubuntu4.2) lenny; urgency=low

  * Non-maintainer upload.
  * Rebuild for lenny

 -- Brian Sutherland <brian@vanguardistas.net>  Fri, 08 May 2009 11:19:43 +0000

python-zope.security (3.4.0-1ubuntu4.1) etch-devel; urgency=low

  * Non-maintainer upload.
  * Rebuild for etch-devel

 -- Brian Sutherland <brian@vanguardistas.net>  Wed, 23 Jul 2008 09:47:19 +0200

python-zope.security (3.4.0-1ubuntu4) hardy; urgency=low

  * Make a new hardy release

 -- Ignas Mikalajunas <ignas.mikalajunas@gmail.com>  Mon, 28 Apr 2008 15:42:24 +0300

python-zope.security (3.4.0-1ubuntu3) gutsy; urgency=low

  * Fix architecture to get 64 bit packages working properly

 -- Ignas Mikalajunas <ignas.mikalajunas@gmail.com>  Mon, 28 Apr 2008 15:42:16 +0300

python-zope.security (3.4.0-1ubuntu2) hardy; urgency=low

  * Re-build for hardy

 -- Ignas Mikalajunas <ignas.mikalajunas@gmail.com>  Wed, 23 Apr 2008 11:19:25 +0300

python-zope.security (3.4.0-1ubuntu1) gutsy; urgency=low

  * Update dependencies

 -- Ignas Mikalajunas <ignas.mikalajunas@gmail.com>  Fri, 11 Apr 2008 20:52:50 +0300

python-zope.security (3.4.0-1) gutsy; urgency=low

  * Inital Release

 -- Brian Sutherland <brian@vanguardistas.net>  Thu,  8 Nov 2007 19:27:48 +0100
